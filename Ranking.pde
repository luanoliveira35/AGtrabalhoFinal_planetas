class ranking {
  //current TOP 10
  int[] geracao = {};
  int[] pontuacao = {};
  int[] colour = {};
  int[] ultima_posicao = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}; //para animação das barras

  float[] geracao_array = {};
  float[] melhor_fitness = {};


  ranking()
  {}


  void atualizar(ArrayList<individuo> placar)
  {
    reset_ultima_posicao();
    if(gerac == 1)
      first_atualizar(placar);
    else
    {
      for(int i=0; i<10 && placar.get(i).get_mais_proximoalvo()>pontuacao[9] ; i++) 
      {
        boolean flag = true;
        for(int j=0; j<10 && flag==true; j++) //
        {
          if(placar.get(i).get_mais_proximoalvo()==pontuacao[j] && placar.get(i).get_color()==colour[j]) // 
          {
            flag = false;
            continue;
          }
          if(placar.get(i).get_mais_proximoalvo()>pontuacao[j])
          {
            for(int k=9; k>j; k--)
            {
              geracao[k]=geracao[k-1];
              pontuacao[k]=pontuacao[k-1];
              colour[k]=colour[k-1];
              ultima_posicao[k]=ultima_posicao[k-1];
            }
            geracao[j]=gerac;
            pontuacao[j]=placar.get(i).get_mais_proximoalvo();
            colour[j]=placar.get(i).get_color();
            ultima_posicao[j]=11;
            flag = false;
          }
        }
      }
    }

    melhor_fitness = append(melhor_fitness, pontuacao[0]);
    geracao_array = append(geracao_array, gerac);
  }


  //Funções locais
  private void first_atualizar(ArrayList<individuo> placar)
  {
    for (int t=0; t<10; t++)
    {
      geracao = append(geracao, 1);
      pontuacao = append(pontuacao, placar.get(t).get_mais_proximoalvo());
      int c = (int) map(placar.get(t).get_mutacao(), min_mutacao, max_mutacao, 850, 0);
      colour = append(colour, c);
    }
      geracao_array = append(geracao_array, 1);
      melhor_fitness = append(melhor_fitness, pontuacao[0]);
  }

  int get_pontuacao(int indice)
  {
    return pontuacao[indice];
  }

  int get_color(int indice)
  {
    return colour[indice];
  }

  int get_geracao(int indice)
  {
    return geracao[indice];
  }

  float[] get_melhor_fitness()
  {
    return melhor_fitness;
  }

  float[] get_geracao_array()
  {
    return geracao_array;
  }

  private void reset_ultima_posicao()
  {
    for(int i=0; i<10; i++)
      ultima_posicao[i]=i;
  }

  int get_ultima_posicao(int indice)
  {
    return ultima_posicao[indice];
  }

} //fim da classe
