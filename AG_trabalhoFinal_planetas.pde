// Bibliotecas gráficas para Chart e cores
import org.gicentre.utils.colour.*;
import org.gicentre.utils.stat.*;
//

PImage fundo;
PImage img;
PImage alvo;
PImage explosao;
PImage planeta;
PImage fire;


//Variáveis Gerais
int num_individuos = 50;
float ang=0;
String cor_colisao_obstaculo = "#FF0000";
public float min_mutacao = 0.0001;
public float max_mutacao = 0.1;
public float anguloalvo ;
public float jitter;
ArrayList<individuo> populacao = new ArrayList<individuo>();
ranking placar = new ranking();
//Variáveis de Estado
int turno = 1; //frame
public int gerac = 1;
int mortes = 0;
int acertos = 0;
public final int turno_max = 2000; // Numero máximo de turnos
//Variaveis do MAPA
int alvo_X = 1060;
int alvo_Y = 250;
int[] bordas = {3, 3, 1077, 503};
int[] obstaculos_x = {180, 420, 220, 820};
int[] obstaculos_y = {250, 400, 670, 405};
int tamanho_circulo = 110;
int margem = 10;
//Plotagem dos Gráficos
ColourTable gradient;
XYChart melhor_fitness; // Gráfico do melhor individuo (quem mais se aproximou do alvo)

void setup() {
  size(1080, 720,P2D);
  frameRate(500);
  img = loadImage("nave.png");
  alvo = loadImage("buraconegro.png");
  fundo = loadImage("space.jpg");
  planeta = loadImage("planeta.png");
  fire = loadImage("fire.png");

  

  //Config
  gradient = ColourTable.readFile(createInput("gradient_0_1020.ctb"));
  melhor_fitness = new XYChart(this);
  // Labels do gráfico Nº de Gerações x Melhor pontuação
  melhor_fitness.showXAxis(true);
  melhor_fitness.showYAxis(true);
  // Cores
  melhor_fitness.setPointSize(2);
  melhor_fitness.setLineWidth(2);
  melhor_fitness.setMinX(1.0001);
  melhor_fitness.setXAxisLabel("geracao");
  melhor_fitness.setYFormat("###");
  melhor_fitness.setXFormat("###");
  melhor_fitness.setLineColour(#FFFFFF);
  melhor_fitness.setPointColour(#FFFFFF);
  
  

  for (int x=0; x<num_individuos; x++)
  {
    populacao.add(new individuo(random(min_mutacao,max_mutacao), bordas));
  }
}


void draw() {
  

  if (turno==1){
    
    println("-----geracao: "+gerac+"-----");
    
  }


  //Desenho do Mapa
 background(fundo);
  //fill(255,255,255);
  noFill();
  stroke(10);
  
  rect(3, 3, 1074, 500);
  fill(0);
  rect(3,505,1074, 210);
  
  
 //for (int ob=0; ob < 4; ob++){
 // pushMatrix();
  //  ang=ang+0.00002;
   // imageMode(CORNER);
  //  translate(width/2,height/2);
   // rotate(ang);
  //  ellipse(obstaculos_x[ob],obstaculos_y[ob], tamanho_circulo,tamanho_circulo);
  //  image(planeta,obstaculos_x[ob],obstaculos_y[ob],tamanho_circulo-5,tamanho_circulo-5);
    //Desenho dos Obstáculos
   // popMatrix();
// }
  
  imageMode(CENTER);
  fill(int(#111111));  // lembrar de mudar no alvo para realizar a colisão (AZUL)
  pushMatrix();
    ang=ang+(1/100);
  
  //ang=ang-0.0002;
  
  ellipse(180, 250, tamanho_circulo,tamanho_circulo);
  image(planeta,180,250,tamanho_circulo-margem,tamanho_circulo-margem);
  popMatrix(); 
  ellipse(420, 250, tamanho_circulo, tamanho_circulo);
  image(planeta,420,250,tamanho_circulo-margem,tamanho_circulo-margem);
  ellipse(420+250, 250-150, tamanho_circulo, tamanho_circulo);
  ellipse(420+250, 250+150, tamanho_circulo, tamanho_circulo);
  image(planeta,420+250,250-150,tamanho_circulo-margem,tamanho_circulo-margem);
  image(planeta,420+250,250+150,tamanho_circulo-margem,tamanho_circulo-margem);
  ellipse(220, 100, tamanho_circulo, tamanho_circulo);
  image(planeta,220,100,tamanho_circulo-margem,tamanho_circulo-margem);
  ellipse(220, 405, tamanho_circulo, tamanho_circulo);
  image(planeta,220,405,tamanho_circulo-margem,tamanho_circulo-margem);
  ellipse(820, 250, tamanho_circulo+35, tamanho_circulo+35);
  image(planeta,820,250,tamanho_circulo+30,tamanho_circulo+30);
  //barras espaciais
  rect(250+711, 5, 30, 160);
  rect(250+711, 340, 30, 160);
  rect(360, 5, 30, 160);
  rect(360, 340, 30, 160);
 
  //Desenha o ALVO
  noStroke();
  fill(#111111);
  image(alvo,alvo_X-70,alvo_Y,120,120);
  smooth();
  //Atualiza Posição e movimento
  atualizarposicaos(populacao, turno, alvo_X, alvo_Y);
 
  //Desenha Foguetes
  for (int g = num_individuos-1; g>=0; g--) //For each individuo
  {
    pushMatrix();
    imageMode(CORNERS);
    translate(populacao.get(g).get_X(), populacao.get(g).get_Y());
 
    rotate(populacao.get(g).get_angulo()-PI);
    /*if(g==0 && gerac!=1)
      //fill(#FCFCFC);
    //else
      {
        float mut = populacao.get(g).get_mutacao();
        int col = (int) map(mut,min_mutacao,max_mutacao,850,0);
        fill(gradient.findColour(col));
      }
    //rect(-4,-8,8,16);
    //triangle(-4, -12, +4, -12, 0, 8);
    */
    image(img, -4, -8, 45, 15); //imagem da nave
    
     popMatrix();
  }

  //Desenho do painel
  fill(255,255,255);
  noStroke();
  textSize(16);
  text("Geração: "+gerac, 400, 530);
  text("Tempo: "+((turno_max-turno)/10), 400, 560);
  text("Players Vivos: "+(num_individuos-mortes), 400, 590);
  text("Acertos: "+ acertos, 400, 620);

  //Desenho do Ranking
  textSize(14);
  //text("1º", 10, 534);
  //text("2º", 10, 554);
  //text("3º", 10, 574);
  for(int i=1; i<=10; i++)
    text(i+"º", 10, 514+(20*i));
  if(gerac!=1)
  {
    int aux = turno;
    if(aux > 100)
      aux = 100;
      stroke(0);
      for(int i=0; i<10; i++)
      {
        fill(#0000FF);
        rect(35, map(aux,0,100,518+(placar.get_ultima_posicao(i)*20),518+(i*20)), ((placar.get_pontuacao(i)-2000)/20), 18);
      }
      fill(#FFFFFF);
      for(int i=0; i<10; i++)
      {
        text(placar.get_pontuacao(i), ((placar.get_pontuacao(i)-2000)/20)+60, map(aux,0,100,534+(placar.get_ultima_posicao(i)*20),534+(i*20)));
        text("("+placar.get_geracao(i)+")" , (((placar.get_pontuacao(i)-2000)/20)+35-int_size(placar.get_geracao(i))*9), map(aux,0,100,533+(placar.get_ultima_posicao(i)*20),533+(i*20)));
      }
    }

  //desenha os graficos e textos
  textSize(12);
  melhor_fitness.draw(600,515,480,200);


  //Checa se é o último turno
  if (turno==turno_max || mortes==num_individuos)
  {
    for (int j = 0; j<num_individuos; j++)
    {
      //Calculo do Fitness
      populacao.get(j).cal_final_fitness(turno_max, alvo_X, alvo_Y);      
    }

    //Ordenar população
    ArrayList<individuo> antigo = populacao;
    populacao = sortList(antigo);

    //Atualizando gráfico
    placar.atualizar(populacao);
    if(gerac==1){  //setar o minimo do gráfico
      melhor_fitness.setMinY(placar.get_pontuacao(9));
    }
    melhor_fitness.setMaxY(placar.get_pontuacao(0));
    melhor_fitness.setData(placar.get_geracao_array(), placar.get_melhor_fitness());

    //Morte dos individuos proporcional ao tamanho da pop
    float morte_individuos = (100/num_individuos)/1; //mata 50%
    /// elimina a tx de mutação, a variação do angulo sempre irá mudar...
    for (int k = num_individuos-1; k>=0 ; k--)
    {
      float rand = random(0, 100);
      if(rand < morte_individuos*k)
      {
        populacao.remove(k);
      }
    }
    int sobreviventes = populacao.size();
    while(populacao.size()<num_individuos)
    {
      float mut = random(1);
      if(mut>0.1)   //10% of 50 ->média de 5 mutações por geração (Taxa de mutacao)
      {
        int rand = (int) random(sobreviventes);
        float new_mutacao = populacao.get(rand).get_mutacao();
        populacao.add(new individuo(new_mutacao, bordas));
      }
      else
        populacao.add(new individuo(random(min_mutacao,max_mutacao), bordas));
    }

    //Organiza novamente (mae e pai podem morrer :(  )
    antigo = populacao;
    populacao = sortList(antigo);

    //gera novos genes
    individuo pai = populacao.get(0);
    individuo mae = populacao.get(1);
    for(int i=2; i<num_individuos; i++)
      populacao.get(i).filho(mae, pai);

    //reseta tudo
    for(int i=0; i<num_individuos; i++){
      populacao.get(i).reset();
    }    
    turno=0;
    mortes=0;
    gerac++;
  }

  turno++;
}





ArrayList<individuo> sortList(ArrayList<individuo> antigo)
{
  ArrayList<individuo> ordenado = new ArrayList<individuo>();
  for (int i = antigo.size()-1; i>=0; i--)
  {
    int max_pontuacao=-1;
    int indice = -1;
    for (int j = antigo.size()-1; j>=0; j--)
    {
      if (antigo.get(j).get_mais_proximoalvo()>max_pontuacao)
      {
        max_pontuacao = antigo.get(j).get_mais_proximoalvo();
        indice = j;
      }
    }
    ordenado.add(antigo.get(indice));
    antigo.remove(indice);
  }
  return ordenado;
}

  int int_size(int num)
  {
    if(num<10)
      return 1;
    if(num<100)
      return 2;
    if(num<1000)
      return 3;
    return 4;
  }
