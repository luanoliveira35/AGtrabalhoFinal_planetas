class individuo{

int x, y;
float angulo;
float gerace[];
int state; //0-Vivo ###-Turno morto      ###-Numero do turno que acertou o alvo
int mais_proximoalvo;  //usado pra armazenazr pontuação
float mutacao;
int bordas[];

/**
Bordas
 */
int borda_x1, borda_x2, borda_y1, borda_y2;


individuo(float mut, int[] bordas)
{
  //Posição Inicial
  x = 50;
  y = 50;
  angulo = 0;
  state = 0;
  gerace = new float[turno_max];
  mais_proximoalvo = 2000; //
  mutacao = mut;
  
  this.borda_x1 = bordas[0];
  this.borda_y1 = bordas[1];
  this.borda_x2 = bordas[2];
  this.borda_x2 = bordas[3];
 

  for (int i=0; i<turno_max; i++)
  {
    gerace[i]=random(-0.2, 0.2);
  }
}

int get_X()
{
  return x;
}

int get_Y()
{
  return y;
}

float get_angulo()
{
  return angulo;
}

void set_X(int xx)
{
  x = xx;
}

void set_Y(int yy)
{
  y = yy;
}

void set_angulo(float ang)
{
  angulo = ang;
}

void set_state(int state1)
{
  state = state1;
}

int get_state()
{
  return state;
}

float get_mutacao()
{
  return mutacao;
}

void set_mutacao(float t)
{
  mutacao = t;
}

int get_color()
{
  return (int) map(mutacao, min_mutacao, max_mutacao, 850, 0);
}

void reset()
{
  x = 50;
  y = 50;
  state = 0;
  angulo = 0;
  mais_proximoalvo = 2000;
}


void handle_mov(int turno)
{
  angulo = angulo + gerace[turno];
  x = x + int(2*cos(angulo));
  y = y + int(2*sin(angulo));
}

int get_mais_proximoalvo()
{
  return mais_proximoalvo;
}

void set_mais_proximoalvo(int dist)
{
  mais_proximoalvo = dist;
}


void random_gerace()
{
  for (int i=0; i<turno_max; i++)
  {
    gerace[i]=random(-0.2, 0.2);
  }
}

float get_gerace_indice(int indice)
{
  return gerace[indice];
}

float[] get_gerace()
{
  return gerace;
}

void set_gerace(float[] g)
{
  for (int i=0; i<turno_max; i++)
    gerace[i]=g[i];
}

void filho(individuo mae, individuo pai)
{
  float random1, random2;
  for (int i=0; i < abs(pai.get_state()) ; i++) //just pass to next geracao the array until dead
  {
    random1=random(0, 1);
    if (random1<0.5)
      gerace[i]=mae.get_gerace_indice(i);
    else
      gerace[i]=pai.get_gerace_indice(i);

    random2=random(0, 1);
    if (random2<mutacao) //mutacaos
      gerace[i]=random(-0.2, 0.2);
  }
  for (int i= abs(pai.get_state())+1; i<turno_max; i++) //random after dead array posicaos (can reduce a lot of not evolution time)
    gerace[i]=random(-0.2, 0.2);
}

/**
 * Calcula a distancia do alvo 
 */
void calc_turno_distancia(float alvo_X, float alvo_Y){
  float dist; //dist to the target
      dist = dist(this.get_X(), this.get_Y(), alvo_X, alvo_Y);
      if (dist < this.get_mais_proximoalvo()) //mais_proximo distancia ever for fitness calculation
      {
        this.set_mais_proximoalvo((int) dist);
      }
  
}

void cal_final_fitness(int turno_max, float alvo_X, float alvo_Y){

      //fitness calculation
      //pontuacao = 1*(turno_max-final_distancia)+0.5*(turno_max-mais_proximo_distancia)+1*(1000-alvo_turno)+0.3*(1000-ultima_of_dead_or_alvo_turno)
      float pontuacao;
      float final_dist = dist(this.get_X(), this.get_Y(), alvo_X, alvo_Y);
      int state = this.get_state(); //turno of dead, positive or negative
      if (state>0) //Acertou o alvo!
      {
        pontuacao = 1.5*(turno_max-final_dist)+1.3*(turno_max-state);
      } else
      {
        if(state != 0) // Não ganha bonus se estiver morto.. 
          pontuacao = (turno_max-final_dist)+0.5*(turno_max-this.get_mais_proximoalvo())+0.3*(turno_max+state);
        else
          pontuacao = (turno_max-final_dist)+0.5*(turno_max-this.get_mais_proximoalvo());
      }
      this.set_mais_proximoalvo((int)pontuacao);
}




}
