ArrayList<individuo> atualizarposicaos(ArrayList<individuo> populacao, int turno, int alvo_X, int alvo_Y){
  num_individuos = populacao.size();
  
  for (int g=0; g<num_individuos; g++) //Para cada individuo
  {
    if (populacao.get(g).get_state()==0) //Tá vivo?
    {
      populacao.get(g).handle_mov(turno-1); //Reposicionar de acordo com o turno
      
      //calcular a distância do alvo no turno atual 
      populacao.get(g).calc_turno_distancia(alvo_X, alvo_Y);
      
      //Condições de Morte
      if (populacao.get(g).get_X()>1077 || populacao.get(g).get_X()<3 || populacao.get(g).get_Y()>503 || populacao.get(g).get_Y()<3 || get(populacao.get(g).get_X(), populacao.get(g).get_Y())==#111111)
      {
        populacao.get(g).set_state(-turno); //negativo para o turno de morte
        mortes++;
      } else
      { //Se acertar o alvo
        //if (get(populacao.get(g).get_X(), populacao.get(g).get_Y())==#111111)
        if (dist(populacao.get(g).get_X(),populacao.get(g).get_Y(),alvo_X,alvo_Y)<=50)
        {
          populacao.get(g).set_state(turno); //positivo para turno de acerto ao alvo
          acertos++;
          mortes++;
        }
      }
    }
  }
  return populacao;
}


individuo calc_turno_distancia(individuo o, float alvo_X, float alvo_Y){
  float dist; //distancia do alvo
      dist = dist(o.get_X(), o.get_Y(), alvo_X, alvo_Y);
      if (dist < o.get_mais_proximoalvo()) //calculo do fitness sempre pelo individuo mais proximo do alvo
      {
        o.set_mais_proximoalvo((int) dist);
      }
  return o;
}
